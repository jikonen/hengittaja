/*
Blink pohjaan rakennettu asetuksen tallennus testailua.
Aluksi vain sarjayhteydeltä tuleva välähdysajan säätö

Käskyinä tällä hetkellä LOWTIME ja HIGHTIME
eli esimerkkinä serialmonitoriin kun kirjottaa

LOWTIME=500

niin pitäisi ledin olla pois päältä 500ms vilkkuessaan


TO DO:

Tämän toiminnan testausta ja korjausta
Sarjayhteys USB:stä kännykän Bluetooth yhteydellä pelaavaan
Pysyvä tallennus FLASH muistiin
    Suoraan tai EEPROM emulointia tai jotain 
        pitää selvittää paljon lisää
Tuulettimen ja ledien ohjaus mukaan ja säätöjen asetukset näille sopivaksi
*/

// *** Muuttujat ja vakiot ***

#define LED RED_LED
#define ARRAYSIZE 32

long lowTime;
long highTime;
char inData[ARRAYSIZE];
char inChar;
byte index = 0;
byte inDigit;
long inNumber = 0;

  
// the setup routine runs once when you press reset:
void setup() {                
    // Aloitetaan sarjayhteys ja testataan toiminta
    Serial.begin(9600);
    Serial.println("Hello");
    // Led pinnin ja muuttujien pohjustus
    pinMode(LED, OUTPUT);
    lowTime = 500;
    highTime = 500;
}

void loop() {
    // *** Kerätään vastaanotettu käskydata ***

    // käskyn lukeminen ja sen toteuttaminen yhden if lauseen taakse
    if(Serial.available()) {
        // Luetaan kaikki saatavilla oleva teksti
        while(Serial.available() > 0) {
            inChar = Serial.read();

            // Tarkistetaan että ei olla välimerkissä tai yli säiliön tilavuuden
            if(inChar != '=' && index < 31) {
                inData[index] = inChar;
                index++;
                inData[index] = '\0'; // Lisätään uudestaan viimeiseksi merkiksi
            } else 
                break; // Välimerkillä tai pituudella rikotaan while loop
        }

        if(inChar == '=') {
            index = 0;
            while(Serial.available() > 0 && index < 10) {
                inDigit = Serial.read() - '0'; 
                // Tämä ilmeisesti muuttaa esim '3' -> 3
                inNumber *= 10; // Kerrotaan olemassaoleva luku 10:llä
                inNumber += inDigit; // Lisätään tuotu luku
                index++;
             }
        }

        /*
        Tässä kohtaa ennen '=' merkkiä tullut teksti on string inData
        ja '=' merkin jälkeen tullut luku on inNumber
        */     

        // *** Suoritetaan komentojen toiminnallisuudet *** 

        if(strcmp(inData, "LOWTIME")) {
            lowTime = inNumber;
            Serial.println("LOWTIME asetettu arvoon");
            Serial.println(lowTime);
        }
        else if(strcmp(inData, "HIGHTIME")) {
            highTime = inNumber;
            Serial.println("HIGHTIME asetettu arvoon");
            Serial.println(highTime);
        }

    }

    digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(highTime);               // wait for a second
    digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
    delay(lowTime);               // wait for a second
}
